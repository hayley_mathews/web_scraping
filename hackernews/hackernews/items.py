# -*- coding: utf-8 -*-

import scrapy


class HackerNewsItem(scrapy.Item):
    title = scrapy.Field()
    article_url = scrapy.Field()
    points = scrapy.Field()
    age = scrapy.Field()
    comment_count = scrapy.Field()
